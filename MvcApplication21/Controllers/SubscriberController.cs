﻿using MvcApplication21.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace MvcApplication21.Controllers
{
    public class SubscriberController : Controller
    {
        //
        // GET: /Subscriber/
        private Entities _entities = new Entities();


        public ActionResult Index()
        {

            return View();

        }

        // GET: /Home/Create 
        [HttpGet]
        public ActionResult Create()
        {

            return View();

        }



        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Create([Bind(Exclude = "UserId")] MvcApplication21.Models.SubscriptionDetail EmployeeToCreate)
        {

            try
            {
                SubscriptionDetail sub = new SubscriptionDetail();
                Subscription subscription = new Subscription();
                // TODO: Add insert logic here 
                MvcApplication21.Models.Entities employeeContext = new Models.Entities();



                if (employeeContext.Subscriptions.Count(x => x.Emailid == WebSecurity.CurrentUserName) > 0)
                {
                    EmployeeToCreate.Email = WebSecurity.CurrentUserName;
                    _entities.SubscriptionDetails.Add(EmployeeToCreate);
                    _entities.SaveChanges();

                }
                else
                {
                    EmployeeToCreate.Email = WebSecurity.CurrentUserName;
                    _entities.SubscriptionDetails.Add(EmployeeToCreate);
                    _entities.SaveChanges();
                    subscription.Emailid = EmployeeToCreate.Email;

                    subscription.Userid = WebSecurity.CurrentUserId;

                    _entities.Subscriptions.Add(subscription);

                    _entities.SaveChanges();
                }



                return RedirectToAction("Index", "Account");



            }

            catch (Exception)
            {

                return View();

            }

        }
    }
}

