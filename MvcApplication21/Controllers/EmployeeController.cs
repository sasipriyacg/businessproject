﻿
using MvcApplication21.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text.RegularExpressions;
using WebMatrix.WebData;

namespace MvcApplication21.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeDBEntities _entities = new EmployeeDBEntities();

        // GET: /Employee/

        //public ActionResult Index()
        //{
        //    return View(_entities.EmployeeProfiles.ToList());
        //}

        public ActionResult HomePage()
        {
            return View();
        }



        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]

        public ActionResult LogIn(Models.EmployeeProfile userr)
        {
            //if (ModelState.IsValid)
            //{
            EmployeeDBEntities employeeDBEntities = new EmployeeDBEntities();
            List<EmployeeProfile> lstEmp = employeeDBEntities.EmployeeProfiles.Where(emp => emp.EMailId == userr.EMailId && emp.Password == userr.Password).ToList();
            if (lstEmp.Count > 0)
            {
                FormsAuthentication.SetAuthCookie(userr.EMailId, false);
                return RedirectToAction("HomePage", "Employee");
            }
            else
            {
                ModelState.AddModelError("", "Login details are wrong.");
            }
            return View(userr);
        }


        // GET: /Home/Create 
        [HttpGet]
        public ActionResult Create()
        {

            return View();

        }

        // POST: /Home/Create 
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Create([Bind(Exclude = "EmpId")] MvcApplication21.Models.EmployeeProfile EmployeeToCreate)
        {

            try
            {

                // TODO: Add insert logic here 
                MvcApplication21.Models.EmployeeDBEntities employeeContext = new Models.EmployeeDBEntities();
                _entities.EmployeeProfiles.Add(EmployeeToCreate);
                _entities.SaveChanges();
                return RedirectToAction("HomePage");

            }

            catch
            {

                return View();

            }

        }

        // GET: /Home/Edit
        [HttpGet]
        public ActionResult Edit(int id)
        {
            EmployeeProfile employee = new EmployeeDBEntities().EmployeeProfiles.Single(emp => emp.EmpId == id);
            return View(employee);

        }

        //

        // POST: /Home/Edit/5 
        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit_Post(EmployeeProfile emp, int id)
        {
            EmployeeDBEntities employeeContext = new EmployeeDBEntities();
            EmployeeProfile employee = employeeContext.EmployeeProfiles.Find(id);
            employee.FirstName = emp.FirstName;
            employee.LastName = emp.LastName;
            employee.EMailId = emp.EMailId;
            employee.Password = emp.Password;
            employee.Gender = emp.Gender;
            employee.PhoneNumber = emp.PhoneNumber;
            employeeContext.Entry(employee).State = EntityState.Modified;

            employeeContext.SaveChanges();

            return RedirectToAction("Index");

            //return View();            
        }
        // GET: /Student/Delete   
        public ActionResult Delete(int id)
        {
            EmployeeProfile employee = _entities.EmployeeProfiles.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
        // POST: /Student/Delete 
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            EmployeeProfile employee = _entities.EmployeeProfiles.Find(id);
            _entities.EmployeeProfiles.Remove(employee);
            _entities.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            _entities.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeProfile employee = _entities.EmployeeProfiles.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Hirek");
        }
    }
}